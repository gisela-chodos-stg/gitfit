package com.stg.gitfit

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class GitFitApplication : Application() {
}