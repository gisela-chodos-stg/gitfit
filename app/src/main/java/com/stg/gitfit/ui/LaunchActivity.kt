package com.stg.gitfit.ui

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.stg.gitfit.databinding.ActivityLaunchBinding
import com.stg.presentation.viewmodels.LaunchViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LaunchActivity : AppCompatActivity() {

    // view binding
    private var _binding: ActivityLaunchBinding? = null
    private val binding get() = _binding!!

    private val viewModel: LaunchViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()

        _binding = ActivityLaunchBinding.inflate(layoutInflater)
        setContentView(binding.root)

    }

}